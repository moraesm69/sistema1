CREATE DATABASE bdClubeTDS;

USE bdClubeTDS;

CREATE TABLE socio(
	id		INT				NOT NULL PRIMARY KEY IDENTITY (1,1),
	nome	VARCHAR (50)	NOT NULL,
	cpf		VARCHAR (14)	NOT NULL 
);

CREATE TABLE dependente(
	id			INT				NOT NULL PRIMARY KEY IDENTITY (1,1),
	nome		VARCHAR (50)	NOT NULL,
	dataNasc	DATE			NOT NULL,
	idSocio		INT				,
	FOREIGN KEY (idSocio) REFERENCES socio(id)

);

INSERT INTO socio (nome,cpf) VALUES ('Matheus', '12545578640');
INSERT INTO socio (nome,cpf) VALUES ('Jonatas', '12545545640');
INSERT INTO socio (nome,cpf) VALUES ('Jonas', '12785545640');


INSERT INTO dependente (nome,dataNasc,idSocio) VALUES ('Matheus', '15/12/1995','1');
INSERT INTO dependente (nome,dataNasc,idSocio) VALUES ('Joyce', '15/12/1995','2');