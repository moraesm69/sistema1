﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Model
{
    public class Socio
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }

        //
        public DataTable Select()
        {
            DataTable socios = new DataTable();



            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = Properties.Settings.Default.ConnectionString;

                    connection.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT * FROM socio;";

                    //executernoquery, executereader , executerscalar
                    //executerreader select com varios (0..n) registros

                    SqlDataReader query = command.ExecuteReader();
                    if (query.HasRows)
                        socios.Load(query);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format(
                        "Erro no Sistema SocioModel -> Select: {0}",
                        ex.Message
                        ),
                    "Aviso",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
            }
            return socios;
        }

    }
}
