﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace View
{
    public partial class TelaDependente : Form
    {
        public Form Tela1 { get; set; }

        public TelaDependente()
        {
            InitializeComponent();
        }

        private void TelaDependente_FormClosing(object sender, FormClosingEventArgs e)
        {
            Tela1.Show();
        }

        private void TelaDependente_Load(object sender, EventArgs e)
        {
            dataGridDependente.DataSource = new Dependente().Select();
        }
    }
}
