﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace View
{
    public partial class TelaSocios : Form
    {
        //Atributo -> Propriedades
        public Form Tela { get; set; }

        public TelaSocios()
        {
            InitializeComponent();
        }

        private void TelaSocios_FormClosing(object sender, FormClosingEventArgs e)
        {
            Tela.Show();
        }

        private void TelaSocios_Load(object sender, EventArgs e)
        {
            dataGridSocios.DataSource = new Socio().Select();
        }
    }
}
