﻿namespace View
{
    partial class TelaDependente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCadastraDependente = new System.Windows.Forms.Button();
            this.btnAtualizarDependente = new System.Windows.Forms.Button();
            this.btnDeletarDependente = new System.Windows.Forms.Button();
            this.btnCancelarDependente = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIdDependente = new System.Windows.Forms.TextBox();
            this.txtNomeDependente = new System.Windows.Forms.TextBox();
            this.dataGridDependente = new System.Windows.Forms.DataGridView();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDependente)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCadastraDependente
            // 
            this.btnCadastraDependente.Location = new System.Drawing.Point(674, 110);
            this.btnCadastraDependente.Name = "btnCadastraDependente";
            this.btnCadastraDependente.Size = new System.Drawing.Size(75, 47);
            this.btnCadastraDependente.TabIndex = 0;
            this.btnCadastraDependente.Text = "Cadastrar";
            this.btnCadastraDependente.UseVisualStyleBackColor = true;
            // 
            // btnAtualizarDependente
            // 
            this.btnAtualizarDependente.Location = new System.Drawing.Point(674, 163);
            this.btnAtualizarDependente.Name = "btnAtualizarDependente";
            this.btnAtualizarDependente.Size = new System.Drawing.Size(75, 47);
            this.btnAtualizarDependente.TabIndex = 1;
            this.btnAtualizarDependente.Text = "Atualizar";
            this.btnAtualizarDependente.UseVisualStyleBackColor = true;
            // 
            // btnDeletarDependente
            // 
            this.btnDeletarDependente.Location = new System.Drawing.Point(674, 233);
            this.btnDeletarDependente.Name = "btnDeletarDependente";
            this.btnDeletarDependente.Size = new System.Drawing.Size(75, 47);
            this.btnDeletarDependente.TabIndex = 2;
            this.btnDeletarDependente.Text = "Deletar";
            this.btnDeletarDependente.UseVisualStyleBackColor = true;
            // 
            // btnCancelarDependente
            // 
            this.btnCancelarDependente.Location = new System.Drawing.Point(674, 286);
            this.btnCancelarDependente.Name = "btnCancelarDependente";
            this.btnCancelarDependente.Size = new System.Drawing.Size(75, 47);
            this.btnCancelarDependente.TabIndex = 3;
            this.btnCancelarDependente.Text = "Cancelar";
            this.btnCancelarDependente.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nome";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(452, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Data de Nascimento";
            // 
            // txtIdDependente
            // 
            this.txtIdDependente.Location = new System.Drawing.Point(34, 57);
            this.txtIdDependente.Name = "txtIdDependente";
            this.txtIdDependente.Size = new System.Drawing.Size(64, 20);
            this.txtIdDependente.TabIndex = 7;
            // 
            // txtNomeDependente
            // 
            this.txtNomeDependente.Location = new System.Drawing.Point(156, 57);
            this.txtNomeDependente.Name = "txtNomeDependente";
            this.txtNomeDependente.Size = new System.Drawing.Size(271, 20);
            this.txtNomeDependente.TabIndex = 8;
            // 
            // dataGridDependente
            // 
            this.dataGridDependente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridDependente.Location = new System.Drawing.Point(34, 110);
            this.dataGridDependente.Name = "dataGridDependente";
            this.dataGridDependente.Size = new System.Drawing.Size(634, 223);
            this.dataGridDependente.TabIndex = 9;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(562, 60);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(106, 20);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // TelaDependente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 378);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dataGridDependente);
            this.Controls.Add(this.txtNomeDependente);
            this.Controls.Add(this.txtIdDependente);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelarDependente);
            this.Controls.Add(this.btnDeletarDependente);
            this.Controls.Add(this.btnAtualizarDependente);
            this.Controls.Add(this.btnCadastraDependente);
            this.Name = "TelaDependente";
            this.Text = "TelaDependente";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TelaDependente_FormClosing);
            this.Load += new System.EventHandler(this.TelaDependente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDependente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCadastraDependente;
        private System.Windows.Forms.Button btnAtualizarDependente;
        private System.Windows.Forms.Button btnDeletarDependente;
        private System.Windows.Forms.Button btnCancelarDependente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdDependente;
        private System.Windows.Forms.TextBox txtNomeDependente;
        private System.Windows.Forms.DataGridView dataGridDependente;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}